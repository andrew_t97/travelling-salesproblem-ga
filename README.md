# Solving the ant48 Travelling Sales Problem using Genetic Algorithms

The code in this repository consists of the following heuristic algorithms which aim to produce optimal solutions to the ant48 TSP:
- Tabu Seach
- Simmulated Annealing
- Genetic Algorithm

## Algorithm perfomance
The average values obtained in this section were achieved after basic tuning of each algorithm's parameters. With more agressive tuning it is possible to achieve results much closer to the optimum route distance of 10628.

### Tabu Search
Tabu search is able to achieve routes with an average total distance of 11226 and a standard deviation of 221. The algorithm has an execution time of around 6 seconds for 30 routes on a high-end PC.

### Simmulated Annealing
Simmulated annealing is much faster than Tabu search and is able to produce 30 routes within less than a second, however, the average distance is 11657 and the standard deviation is around 291.

### Genetic algorithm
The genetic algorithm uses a roulette style selection method and breeds different solutions using the two-opt algorithm. The genetic algorithm creates routes of an average distance of 11084 with a standard deviation of 203. The algorithm is much slower than both Tabu and Simmulated annealing, however, it outperforms both in terms of the quality of solution.

## The program itself
Currently the program requires compilation and is limited to Windows 10 machines. There is also no way of tuning the parameters of each algorithm at this current time but this can be changed in the future. The program offers the ability to run each algorithm once or 30 times and create an average and standard deviation. The genetic algorithm can also be run in multiple threads to reduce the execution time from around 25 minutes on a single thread. There is also a python script included which allows the routes created in a run to be visualised and played back. The python script reads in a results file which can be exported from the main program.

### Genetic algorithm playback
[![Image from Gyazo](https://i.gyazo.com/e7f4276c2d1e0e6bd6df45c405b28bf5.gif)](https://gyazo.com/e7f4276c2d1e0e6bd6df45c405b28bf5)