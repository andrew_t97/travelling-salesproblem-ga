#pragma once
#include <vector>
#include "Data_Collection.h"

// Contains data about a given route e.g. distance, rank, weight, and route itself.
class Route {
public:
    std::vector<int> route;
    int distance;
    double weight = 0;

    Route(std::vector<int> route, Data_Collection* data_manipulator) : route(route) {
        distance = data_manipulator->calc_route_distance(&route);
    }

    bool operator==(const Route& rhs) {
        for (unsigned int i = 0; i < route.size(); ++i) {
            if (route[i] != rhs.route[i]) {
                return false;
            }
        }

        return true;
    }
};