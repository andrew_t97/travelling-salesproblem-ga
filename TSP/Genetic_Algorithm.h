#pragma once
#include "TSP_Solver.h"
#include "Route.h"
#include <thread>
#include <future>
class Genetic_Algorithm : public TSP_Solver {
	void rank_routes(std::vector<Route>* population);
	std::vector<Route> create_population(int population_size);
	void selection(std::vector<Route>* routes, unsigned int elite_size);
	Route breed(const std::vector<int>* parent1, const std::vector<int>* parent2);
	std::vector<Route> breed_population(std::vector<Route>* mating_pool, int elite_size);
	std::vector<Route> next_generation(std::vector<Route> current_gen, int elite_size);
	std::vector<std::pair<int, std::vector<int>>> run_ga(int num_of_iterations, std::atomic<int>* iterations_complete);

public:
	Genetic_Algorithm(Data_Collection* data_manipulator) : TSP_Solver(data_manipulator) {}
	std::pair<int, std::vector<int>> genetic_algorithm(const int population_size, const int elite_size, const int generations);
	std::pair<double, double> average_genetic_algorithm();
	std::pair<double, double> threaded_average_ga(const int num_of_threads, const int num_of_iterations = 30, bool should_export=false);
};

