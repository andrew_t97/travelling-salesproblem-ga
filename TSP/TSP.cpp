#include <iostream>
#include <stdio.h>
#include "Tabu_Search.h"
#include "Simmulated_Annealing.h"
#include "Data_Collection.h"
#include "Genetic_Algorithm.h"
#include "Cmd_frontend.h"

int main()
{
    Cmd_frontend::run_frontend();
    return 0;
}

