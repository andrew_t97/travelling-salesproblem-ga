from os import path
import urllib.request
import matplotlib.pyplot as plt
import numpy as np
import re
import time
import tkinter
import argparse

def read_file(filepath):
    """Reads in the data file and parses it into a list of distances and routes

    Arguments:
        filepath {string} -- The path to the file

    Returns:
        list of tuples -- Returns a list of tuples containing a distance and it's corresponding route
    """
    if (filepath is None or not path.exists(filepath)):
        print("Provided filepath does not exist!")
        return

    results_file = open(filepath, 'r')

    routes = []
    current_distance = 0
    current_route = []
    for line in results_file:
        # Removing trailing newlines
        line = line.rstrip() 
        if "DISTANCE" in line:
            current_distance = int(line.split()[1])
        elif line.isdigit():
            current_route.append(int(line))
        elif line == "END" :
            routes.append((current_distance, current_route))
            current_route = []

    return routes

def read_in_data_file():
    """Reads in data from a coordinate data url
    
    Arguments:
        url {string} -- The url of the website where the coordinate data is stored
    
    Returns:
        list -- The list of the coordinates of all cities
    """
    DATA_URL = "http://elib.zib.de/pub/mp-testdata/tsp/tsplib/tsp/att48.tsp"
    data = []
    for line in urllib.request.urlopen(DATA_URL):
        line_str = line.decode("utf-8") 
        if re.match("^[0-9 ]+$", line_str):
            split_str = line.split()
            data_tuple = (int(split_str[1]), int(split_str[2]))
            data.append(data_tuple)

    return data   

def play_back_results(results):
    # Sorting list by distance
    sorted_results = sorted(results, key=lambda result: result[0], reverse=True)
    coord_data = read_in_data_file()

    x, y = [], []

    # Getting worst route's data
    for position in sorted_results[0][1]:
        current_position = coord_data[position - 1]
        x.append(current_position[0])
        y.append(current_position[1])
    
    try :
        # Setting up the graph
        plt.ion()
        fig, ax = plt.subplots()
        sc = ax.scatter(x, y)
        lines, = ax.plot(x, y)
        start_annotation = ax.annotate('Start', xy=(x[0], y[0]), xytext=(0, 0), color='red', textcoords='offset points')
        end_annotation = ax.annotate('End', xy=(x[len(x) - 1], y[len(y) - 1]), xytext=(0, 0), color='red', textcoords='offset points')
        plt.title("Distance: " + str(sorted_results[0][0]))
        plt.draw()
        
        if (len(sorted_results) == 1):
            plt.waitforbuttonpress()
            exit(0)

        # Iterating over each route and plotting it to the graph
        for route in sorted_results:
            x = []
            y = []
            for position in route[1]:
                current_position = coord_data[position - 1]
                x.append(current_position[0])
                y.append(current_position[1])

            # Updating the position of the points on the graph to the new route
            sc.set_offsets(np.c_[x, y])
            lines.set_data(x,y)
            start_annotation.xy = (x[0], y[0])
            end_annotation.xy = (x[len(x) - 1], y[len(y) - 1])
            plt.title("Distance: " + str(route[0])) # Updating title with new distance value
            fig.canvas.draw_idle()
            plt.pause(2)

        plt.waitforbuttonpress()
    except tkinter.TclError:
        exit(0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--path", help="Filepath to the results you wish to view")
    args = parser.parse_args()
    
    data = read_file(args.path)

    if data is not None:
        play_back_results(data)