#include "Data_Collection.h"
#include "curl/curl.h"
#include <iostream>
#include <stdio.h>
#include <sstream>
#include <regex>
#include <vector>
#include <cmath>
#define CURL_STATICLIB

size_t Data_Collection::writeFunction(void* ptr, size_t size, size_t nmemb, std::string* data) {
    data->append((char*)ptr, size * nmemb);
    return size * nmemb;
}

// Reads data from a URL and converts it into a single string
std::string  Data_Collection::get_data(const std::string* url) {
    CURL* curl;
    CURLcode res;
    std::string response_string;
    std::string header_string;

    curl_global_init(CURL_GLOBAL_DEFAULT);

    curl = curl_easy_init();
    if (curl) {

        curl_easy_setopt(curl, CURLOPT_URL, (*url).c_str()); /*Providing null terminated url string*/
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeFunction);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response_string);

        /* Perform the request, res will get the return code */
        res = curl_easy_perform(curl);
        /* Check for errors */
        if (res != CURLE_OK)
            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));

        /* always cleanup */
        curl_easy_cleanup(curl);
    }

    curl_global_cleanup();
    return response_string;
}

// Checks if the provided character array consists of only digits and spaces
bool Data_Collection::is_digits_or_spaces(const char* buffer) {
    while (*buffer) {
        if (!isdigit(*buffer) && !isspace(*buffer)) {
            return false;
        }
        ++buffer;
    }
    return true;
}

// Reads in a string of coordinate data and returns it as a vector of coordinates, each position in the vector corresponding to a different city
std::vector<std::pair<int, int>> Data_Collection::get_coordinates(const std::string* data) {
    std::istringstream f(*data);
    std::string line;
    std::vector<std::pair<int, int>> coordinates;
    while (getline(f, line)) {
        if (is_digits_or_spaces(line.c_str())) {

            // Using string stream to iterate over words allowing each seperate number to be iterated over rather than each character
            std::istringstream ssin(line);
            std::string digit;
            int digits[2];

            // Iterating over each number in the line split by spaces, starts at -1 in order to avoid the city ID
            for (int i = -1; i < 2; ++i) {
                if (!ssin) break;
                else if (i < 0) {
                    ssin >> digit;
                    continue;
                }

                ssin >> digit;
                digits[i] = (std::stoi(digit));
                digit = "";
            }
            coordinates.push_back(std::make_pair(digits[0], digits[1]));
        }
    }
    return coordinates;
}

// Reads in string data for the optimal route (solution) and returns a vector of the cities in the order they appear in the optimal route
std::vector<int> Data_Collection::get_optimal_route(const std::string* data) {
    std::istringstream f(*data);
    std::string line;
    std::vector<int> route;

    while (getline(f, line)) {

        if (std::all_of(line.begin(), line.end(), ::isdigit)) {
            route.push_back(std::stoi(line));
        }
    }

    return route;
}

/* Calculates and returns the euclidean distance between two coordinates */
int Data_Collection::calc_euclidean_distance(const std::pair<int, int>* a, std::pair<int, int>* b) {
    int x_dist = a->first - b->first;
    int y_dist = a->second - b->second;

    double raw_dist = sqrt(((x_dist * x_dist) + (y_dist * y_dist)) / 10);
    int t_ab = int(raw_dist);
    int dist_ab = t_ab;

    if (t_ab < raw_dist) {
        dist_ab = t_ab + 1;
    }

    return dist_ab;
}

// Parses the coordinate and optimal route data from the urls into usable data structures
Data_Collection::Data_Collection(const std::string* coordinate_data_url, const std::string* optimal_route_url) {
    coord_data = get_coordinates(&(get_data(coordinate_data_url)));
    optimal_route = get_optimal_route(&(get_data(optimal_route_url)));
}

// Calculates the total euclidean distance between each point of a provided route and back to the start again
int Data_Collection::calc_route_distance(const std::vector<int>* tour_ptr) {
    const std::vector<int>& tour_ref = *tour_ptr;
    int total_distance = 0;
    int prev_pos = tour_ref[0] - 1;
    int target_pos;

    for (unsigned int i = 1; i < tour_ref.size(); ++i) {
        target_pos = tour_ref[i] - 1;
        total_distance += calc_euclidean_distance(&coord_data[prev_pos], &coord_data[target_pos]);
        prev_pos = target_pos;
    }

    // Finding distance from the last city back to the first city
    prev_pos = tour_ref[tour_ref.size() - 1] - 1;
    target_pos = tour_ref[0] - 1;
    total_distance += calc_euclidean_distance(&coord_data[prev_pos], &coord_data[target_pos]);

    return total_distance;
}