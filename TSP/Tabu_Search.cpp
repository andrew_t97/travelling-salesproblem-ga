#include <random>
#include "Tabu_Search.h"
#include "progresscpp/ProgressBar.hpp"

// Checks for neighbour solutions of the current route (solution) using the two-opt algorithm
std::vector<Route> Tabu_Search::get_neighbours(const Route* current_route) {
    std::vector<Route> neighbour_solutions;
    std::random_device rd;
    std::mt19937 eng(rd());
    std::uniform_int_distribution<> distr(0, 47);

    for (unsigned int i = 0; i < current_route->route.size(); ++i) {
        int current_city_index = i;
        int city_to_swap_index = distr(eng);
        while (city_to_swap_index == current_city_index) city_to_swap_index = distr(eng);

        int x = std::min(current_city_index, city_to_swap_index);
        int y = std::max(current_city_index, city_to_swap_index);

        neighbour_solutions.push_back(Route(two_opt(&(current_route->route), x, y), data_manipulator));
    }

    return neighbour_solutions;
}

// Carries out tabu search starting with a random solution. Returns the best solution's distance and route as a pair
std::pair<int, std::vector<int>> Tabu_Search::tabu_search(unsigned int tabu_size) {
    const int NUM_VALUES = 48;
    const int ITERATIONS = 3000;
    Route best_solution = Route(generate_rand_solution(NUM_VALUES), data_manipulator);
    std::vector<Route> tabu;

    for (int i = 0; i < ITERATIONS; ++i) {
        Route current_solution = best_solution;
        std::vector<Route> neighbours = get_neighbours(&current_solution);

        for (Route neighbour : neighbours) {

            // Checking if new solutions are better than best solution and are not in the tabu
            if (std::find(tabu.begin(), tabu.end(), neighbour) == tabu.end() && neighbour.distance < best_solution.distance) {
                best_solution = neighbour;
                tabu.push_back(neighbour);
            }

            while (tabu.size() > tabu_size) {
                tabu.erase(tabu.begin());
            }
        }
    }
    return std::make_pair(best_solution.distance, best_solution.route);
}

/* Carries out tabu search a set number of times and returns the average
   distance of the best solution of each iteration and the standard deviation
   of all iterations */
std::pair<double, double> Tabu_Search::tabu_average(bool should_export) {
    std::vector<std::pair<int, std::vector<int>>> final_results;
    int NUM_OF_ITERATIONS = 30;

    progresscpp::ProgressBar progress_bar(NUM_OF_ITERATIONS, 70);
    for (int i = 0; i < NUM_OF_ITERATIONS; ++i) {
        final_results.push_back(tabu_search(16));
        ++progress_bar;
        progress_bar.display();
    }
    progress_bar.done();

    int sum_of_distances = 0;
    for (auto& n : final_results) sum_of_distances += n.first;
    double average_distance = sum_of_distances / 30;

    double total = 0;
    for (auto distance : final_results) total += (std::pow((distance.first - average_distance), 2.0));
    double standard_dev = std::sqrt(total / 30);

    if (should_export) std::cout << "Stored results in the following file: " << export_to_file(final_results, "Tabu_Search") << std::endl;

    return std::make_pair(std::round(average_distance), std::round(standard_dev));
}