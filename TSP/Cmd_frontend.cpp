#include "Cmd_frontend.h"
#include <iostream>
#include <string>
#include "Data_Collection.h"
#include "Simmulated_Annealing.h"
#include "Tabu_Search.h"
#include "Genetic_Algorithm.h"

void Cmd_frontend::run_frontend() {
	bool valid_response = false;
	bool exit = false;

	std::string DATA_URL = "http://elib.zib.de/pub/mp-testdata/tsp/tsplib/tsp/att48.tsp";
	std::string OPTIMAL_TOUR = "http://elib.zib.de/pub/mp-testdata/tsp/tsplib/tsp/att48.opt.tour";
	std::string response;
	std::string possible_commands[] = {"SA", "T", "TA", "G", "GA", "GAT", "exit"};
	std::string commands = "- S, Run the Simmulated Annealing algorithm once\n"
		"- SA, Run the Simulated Annealing algorithm 30 time and display the average and standard deviation\n"
		"- T, Run the Tabu Search algorithm once\n"
		"- TA, Run the Tabu Search algorithm 30 times and display the average and standard deviation\n"
		"- G, Run the Genetic Algorithm once\n"
		"- GA, Run the Genetic Algorithm 30 times and display the average and standard deviation\n"
		"- GAT, Run the Genetic Algorithm 30 times with multiple threads and display the average and standard deviation\n"
		"- H, Shows this messages\n"
		"- exit, Exits the program\n";

	Data_Collection data_manipulator = Data_Collection(&DATA_URL, &OPTIMAL_TOUR);

	std::cout << "\nThe following are the commands available:" << std::endl;
	std::cout << commands << std::endl;

	while (!exit) {		
		std::cout << "> ";
		std::getline(std::cin, response);

		if (response.compare("S") == 0) {
			Simmulated_Annealing sa(&data_manipulator);
			std::pair<int, std::vector<int>> results = sa.simulated_annealing(20, 0.3);
			std::cout << "Results:\n	Distance = " << results.first << std::endl;
		}
		else if (response.compare("SA") == 0) {
			bool export_results = NULL;
			bool responded = false;
			
			do {
				std::cout << "Do you wish to export the results? (y/n)";
				getline(std::cin, response);
				if (response.compare("y") == 0) {
					export_results = true;
					responded = true;
				} 
				else if (response.compare("n") == 0) {
					export_results = false;
					responded = true;
				}
			} while (!responded);


			Simmulated_Annealing sa(&data_manipulator);
			std::pair<double, double> results = sa.average_simulated_annealing(export_results);
			std::cout << "Results:\n	Average distance = " << results.first << ", Standard deviation = " << results.second << std::endl;
		}

		else if (response.compare("T") == 0) {
			Tabu_Search ta(&data_manipulator);
			std::pair<int, std::vector<int>> results = ta.tabu_search(16);
			std::cout << "Results:\n	Distance = " << results.first << std::endl;
		}
		else if (response.compare("TA") == 0) {
			bool export_results = NULL;
			bool responded = false;
			
			do {
				std::cout << "Do you wish to export the results? (y/n)";
				getline(std::cin, response);
				if (response.compare("y") == 0) {
					export_results = true;
					responded = true;
				}
				else if (response.compare("n") == 0) {
					export_results = false;
					responded = true;
				}
			} while (!responded);


			Tabu_Search ta(&data_manipulator);
			std::pair<double, double> results = ta.tabu_average(export_results);
			std::cout << "Results:\n	Average distance = " << results.first << ", Standard deviation = " << results.second << std::endl;
		}

		else if (response.compare("G") == 0) {
			Genetic_Algorithm ga(&data_manipulator);
			std::pair<int, std::vector<int>> results = ga.genetic_algorithm(1500, 220, 3000);
			std::cout << "Results:\n	Distance = " << results.first << std::endl;
		}
		else if (response.compare("GA") == 0) {
			bool export_results = NULL;
			bool responded = false;
			
			do {
				std::cout << "Do you wish to export the results? (y/n)";
				getline(std::cin, response);
				if (response.compare("y") == 0) {
					export_results = true;
					responded = true;
				}
				else if (response.compare("n") == 0) {
					export_results = false;
					responded = true;
				}
			} while (!responded);


			Genetic_Algorithm ga(&data_manipulator);
			std::pair<double, double> results = ga.average_genetic_algorithm();
			std::cout << "Results:\n	Average distance = " << results.first << ", Standard deviation = " << results.second << std::endl;
		}
		else if (response.compare("GAT") == 0) {
			bool export_results = NULL;
			bool responded = false;

			do {
				std::cout << "Do you wish to export the results? (y/n)";
				getline(std::cin, response);
				if (response.compare("y") == 0) {
					export_results = true;
					responded = true;
				}
				else if (response.compare("n") == 0) {
					export_results = false;
					responded = true;
				}
			} while (!responded);


			Genetic_Algorithm ga(&data_manipulator);
			std::pair<double, double> results = ga.threaded_average_ga(6, 30, export_results);
			std::cout << "Results:\n	Average distance = " << results.first << ", Standard deviation = " << results.second << std::endl;
		}

		else if (response.compare("H") == 0) {
			std::cout << "\nThe following are the commands available:" << std::endl;
			std::cout << commands << std::endl;
		}

		else if (response.compare("exit") == 0) {
			exit = true;
		}

		else {
			std::cout << response << " is not a valid command!" << std::endl;
		}
	}
}