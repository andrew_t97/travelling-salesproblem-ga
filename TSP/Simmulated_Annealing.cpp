#include <random>
#include "Simmulated_Annealing.h"
#include "progresscpp/ProgressBar.hpp"

// Returns the probability that the new route should be accepted in SA
double Simmulated_Annealing::get_accept_prob(int new_distance, int current_distance, double temperature) {
    if (new_distance < current_distance) return 1.0;
    else return std::exp((double(current_distance) - double(new_distance)) / temperature);
}

std::pair<int, std::vector<int>> Simmulated_Annealing::simulated_annealing(double temperature, double cooling_rate) {
    std::vector<int> best_solution = generate_rand_solution(48);
    std::vector<int> current_solution = best_solution;
    std::vector<int> new_solution;

    int best_distance = data_manipulator->calc_route_distance(&best_solution);
    int current_distance = best_distance;
    int new_distance = 0;

    const double initial_temp = temperature;
    double current_temp = temperature;

    const int ITERATIONS = 3000;

    std::random_device rd;
    std::mt19937 eng(rd());
    std::uniform_int_distribution<> city_distr(0, 47);
    std::uniform_real_distribution<> acc_distr(0, 1);

    for (int i = 0; i < ITERATIONS; ++i) {
        if (current_temp <= 0) break;

        /* Generating indexes of cities within route randomly */
        int first_city = city_distr(eng);
        int second_city = city_distr(eng);
        while (first_city == second_city) second_city = city_distr(eng);

        int x = std::min(first_city, second_city);
        int y = std::max(first_city, second_city);

        // Creating new solution using two-opt algorithm
        new_solution = two_opt(&current_solution, x, y);
        new_distance = data_manipulator->calc_route_distance(&new_solution);

        // Calculating and assessing the probability of using the new solution as the current solution 
        if (get_accept_prob(new_distance, current_distance, current_temp) > acc_distr(eng)) {
            current_solution = new_solution;
            current_distance = new_distance;

            if (current_distance < best_distance) {
                best_distance = current_distance;
                best_solution = current_solution;
            }
        }

        current_temp = initial_temp / (1 + (i * cooling_rate));
    }

    return std::make_pair(best_distance, best_solution);
}

/* Carries out simulated annealing a set number of times and returns the average
   distance of the best solution of each iteration and the standard deviation
   of all iterations */
std::pair<double, double> Simmulated_Annealing::average_simulated_annealing(bool should_export) {
    int NUM_OF_ITERATIONS = 30;
    std::vector<std::pair<int, std::vector<int>>> final_results;

    progresscpp::ProgressBar progress_bar(NUM_OF_ITERATIONS, 70);
    for (int i = 0; i < NUM_OF_ITERATIONS; ++i) {
        final_results.push_back(simulated_annealing(20, 0.3));
        ++progress_bar;
        progress_bar.display();
    }
    progress_bar.done();

    int sum_of_distances = 0;
    for (auto result : final_results) sum_of_distances += result.first;
    double average_distance = sum_of_distances / 30;

    double total = 0;
    for (auto result : final_results) total += (std::pow((result.first - average_distance), 2.0));
    double standard_dev = std::sqrt(total / 30);

    if (should_export) std::cout << "Stored results in the following file: " << export_to_file(final_results, "Simulated_Annealing") << std::endl;

    return std::make_pair(std::round(average_distance), std::round(standard_dev));
}