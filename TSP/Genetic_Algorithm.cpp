#include "Genetic_Algorithm.h"
#include <iostream>
#include <stdio.h>
#include <random>
#include <chrono>
#include "progresscpp/ProgressBar.hpp"

// Sorts the given population by the distance of route in descending order (hopefully!)
void Genetic_Algorithm::rank_routes(std::vector<Route>* population) {
    std::sort(population->begin(), population->end(), [](const Route& lhs, const Route& rhs) {
        return lhs.distance < rhs.distance;
        });
}

// Creates a vector of randomly generated routes
std::vector<Route> Genetic_Algorithm::create_population(int population_size) {
    std::vector<Route> population;

    for (int i = 0; i < population_size; ++i) {
        population.push_back(Route(generate_rand_solution(48), data_manipulator));
    }

    rank_routes(&population);
    return population;
}

// Selects the elite individuals from the provided population and also randomly selects poorer performing individuals
void Genetic_Algorithm::selection(std::vector<Route>* routes, unsigned int elite_size) {
    std::vector<Route> selection_results;

    std::random_device rd;
    std::mt19937 eng(rd());
    std::uniform_real_distribution<> acc_distr(0, 1);

    int total_distance = 0;
    for (auto& route : *routes) total_distance += route.distance;
    for (auto& route : *routes) route.weight = route.distance / total_distance;

    for (unsigned int i = 0; i < routes->size(); ++i) {
        (*routes)[i].weight = (*routes)[i].distance / total_distance;
    }

    // Selecting individuals for mutation later
    int current_pos = 0;
    for (unsigned int i = 0; i < routes->size(); ++i) {
        if (i < elite_size || (*routes)[i].weight < acc_distr(eng)) {
            (*routes)[current_pos] = (*routes)[i];
            ++current_pos;
        }
    }

    // Removing unselected values from route vector
    for (unsigned int i = current_pos; i < routes->size(); ++i) {
        routes->erase(routes->begin() + i);
    }
}

/* Takes two parents and carries out ordered crossover, taking a portion of parent 1 and then filling in the rest
   of the route with parent 2 */
Route Genetic_Algorithm::breed(const std::vector<int>* parent1, const std::vector<int>* parent2) {
    std::vector<int> child(parent1->size(), NULL);
    std::random_device rd;
    std::mt19937 eng(rd());
    std::uniform_real_distribution<> acc_distr(0, 1);

    unsigned int gene_a = int(acc_distr(eng) * parent1->size());
    unsigned int gene_b = int(acc_distr(eng) * parent1->size());

    unsigned int start_gene = std::min(gene_a, gene_b);
    unsigned int end_gene = std::max(gene_a, gene_b);

    // Filling child with portion of parent 1 genes
    for (unsigned int i = 0; i <= end_gene; ++i) child[i] = (*parent1)[i];

    // Filling in the rest of child with genes from parent 2
    for (unsigned int i = 0; i < child.size(); ++i) {
        if (i >= gene_a && i <= gene_b) continue;

        for (unsigned int j = 0; j < parent2->size(); ++j) {
            if (std::find(child.begin(), child.end(), (*parent2)[j]) != child.end()) continue;
            else {
                child[i] = (*parent2)[j];
                break;
            }
        }
    }

    return Route(child, data_manipulator);
}

/* Takes the routes of the provided population and breeds them via ordered crossover and adds them to the new generation
   with the elite individuals*/
std::vector<Route> Genetic_Algorithm::breed_population(std::vector<Route>* mating_pool, int elite_size) {
    std::vector<Route> children;
    std::random_device rd;
    std::mt19937 eng(rd());
    int length = mating_pool->size() - elite_size;

    /* Adding our elite individuals to the children */
    for (int i = 0; i < elite_size; ++i) children.push_back((*mating_pool)[i]);

    /* Shuffling the mating pool into a random order and then breeding individuals together */
    std::shuffle(mating_pool->begin(), mating_pool->end(), eng);
    for (int i = 0; i < length; ++i) {
        Route child = breed(&(*mating_pool)[i].route, &(*mating_pool)[mating_pool->size() - i - 1].route);
        children.push_back(child);
    }

    return children;
}

// Creates the next generation of individuals by carrying out ranking, selection, and breeding. Assumes current_gen is already sorted by distance in ascending order
std::vector<Route> Genetic_Algorithm::next_generation(std::vector<Route> current_gen, int elite_size) {
    selection(&current_gen, elite_size);
    std::vector<Route> new_pop = breed_population(&current_gen, elite_size);
    rank_routes(&new_pop);
    return new_pop;

    return current_gen;
}

/* Carries out the genetic algorithm with the provided parameters */
std::pair<int, std::vector<int>> Genetic_Algorithm::genetic_algorithm(const int population_size, const int elite_size, const int generations) {
    std::vector<Route> current_generation = create_population(population_size);
    int current_best_distance = -1;
    std::vector<int> current_best_route;

    for (int i = 0; i < generations; ++i) {
        current_generation = next_generation(current_generation, elite_size);

        if (current_best_distance < 0 || current_generation[0].distance < current_best_distance) {
            current_best_distance = current_generation[0].distance;
            current_best_route = current_generation[0].route;
        }
    }

    return std::make_pair(current_best_distance, current_best_route);
}

/* Carries out the genetic algorithm a set number of times and returns the average
    distance of the best solution of each iteration and the standard deviation
    of all iterations */
std::pair<double, double> Genetic_Algorithm::average_genetic_algorithm() {
    std::vector<int> distances;
    int NUM_OF_ITERATIONS = 30;

    progresscpp::ProgressBar progress_bar(NUM_OF_ITERATIONS, 70);
    for (int i = 0; i < NUM_OF_ITERATIONS; ++i) {
        distances.push_back(genetic_algorithm(1500, 220, 3000).first);
        ++progress_bar;
        progress_bar.display();
    }
    progress_bar.done();

    int sum_of_distances = 0;
    for (auto& n : distances) sum_of_distances += n;
    double average_distance = sum_of_distances / 30;

    double total = 0;
    for (int distance : distances) total += (std::pow((distance - average_distance), 2.0));
    double standard_dev = std::sqrt(total / 30);

    return std::make_pair(std::round(average_distance), std::round(standard_dev));
}

// Runs the genetic algorithm for a set number of iterations and returns a vector containing the results of each iteration
std::vector<std::pair<int, std::vector<int>>> Genetic_Algorithm::run_ga(int num_of_iterations, std::atomic<int>* iterations_complete) {
    std::vector<std::pair<int, std::vector<int>>> results;

    for (int i = 0; i < num_of_iterations; ++i) {
        results.push_back(genetic_algorithm(1500, 220, 3000));
        ++(*iterations_complete);
    }

    return results;
}

/* Carries out the genetic algorithm in a multithreaded fashion reducing the runtime greatly. Higher thread numbers improve runtime but
    increase processor usage greatly. The number of iterations indicates how many iterations should be carried out before an average distance
    and standard deviation is calculated.*/

std::pair<double, double> Genetic_Algorithm::threaded_average_ga(const int num_of_threads, const int num_of_iterations, bool should_export) {

    int no_threads = num_of_threads;
    if (no_threads > num_of_iterations) no_threads = num_of_iterations;
    int iterations_per_thread = num_of_iterations / no_threads;
    int remainder_iterations = num_of_iterations % no_threads;
    int total_distance = 0;
    int current_iterations;
    std::atomic<int> iterations_complete = 0;

    // Creating a list of threads which will run the ga algorithm
    std::vector<std::future<std::vector<std::pair<int, std::vector<int>>>>> ga_threads;
    std::vector<std::pair<int, std::vector<int>>> final_results;

    // Calculating how many iterations should be assigned to each thread and carrying out that assignment
    for (int i = 0; i < no_threads; ++i) {
        current_iterations = iterations_per_thread;

        if (remainder_iterations > 0) {
            ++current_iterations;
            --remainder_iterations;
        }

        ga_threads.push_back(std::async(std::launch::async, &Genetic_Algorithm::run_ga, this, current_iterations, &iterations_complete));
    }

    progresscpp::ProgressBar progress_bar(num_of_iterations, 70);

    // Reading the number of iterations which have been completed by the threads and updating the progress bar
    do {
        progress_bar.set_ticks(iterations_complete);
        progress_bar.display();
        std::this_thread::sleep_for(std::chrono::seconds(1));
    } while (iterations_complete != num_of_iterations);

    progress_bar.set_ticks(iterations_complete);
    progress_bar.done();

    // Fetching the results from the threads running the ga
    for (unsigned int i = 0; i < ga_threads.size(); ++i) {
        auto current_results = ga_threads[i].get();
        final_results.insert(final_results.end(), current_results.begin(), current_results.end());
    }

    // Calculating average distance and standard deviation
    int sum_of_distances = 0;
    for (auto& result : final_results) sum_of_distances += result.first;
    double average_distance = sum_of_distances / num_of_iterations;

    double total = 0;
    for (auto& result : final_results) total += (std::pow((result.first - average_distance), 2.0));
    double standard_dev = std::sqrt(total / num_of_iterations);

    if (should_export) std::cout << "Stored results in the following file: "<< export_to_file(final_results, "Genetic_alg_threaded") << std::endl;

    return std::make_pair(average_distance, standard_dev);
}