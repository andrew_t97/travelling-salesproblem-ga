#pragma once
#include "TSP_Solver.h"
class Simmulated_Annealing : public TSP_Solver {
	double get_accept_prob(int new_distance, int current_distance, double temperature);

public:
	Simmulated_Annealing(Data_Collection* data_manipulator) : TSP_Solver(data_manipulator) {}
	std::pair<int, std::vector<int>> simulated_annealing(double temperature, double cooling_rate);
	std::pair<double, double> average_simulated_annealing(bool should_export);
};

