#pragma once
#include<vector>
#include<string>
#include <iostream>
#include <fstream>
#include "Data_Collection.h"
#include "Route.h"

class TSP_Solver
{
protected:
	Data_Collection* data_manipulator;
	void test_euclidean_dist();
	std::vector<int> two_opt(const std::vector<int>* route, int city_index_1, int city_index_2);
	std::vector<int> generate_rand_solution(int solution_size);
	std::string export_to_file(std::vector<std::pair<int, std::vector<int>>> routes, std::string prefix);

public:
	TSP_Solver(Data_Collection* data_manipulator) : data_manipulator(data_manipulator) {}

	
};

