#pragma once
#include "TSP_Solver.h"
#include "Route.h"

class Tabu_Search : public TSP_Solver {

	std::vector<Route> get_neighbours(const Route* current_route);
public:
	Tabu_Search(Data_Collection* data_manipulator) : TSP_Solver(data_manipulator){}
	std::pair<int, std::vector<int>> tabu_search(unsigned int tabu_size);
	std::pair<double, double> tabu_average(bool should_export);

};

