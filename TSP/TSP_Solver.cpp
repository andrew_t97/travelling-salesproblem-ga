#include "TSP_Solver.h"
#include <random>
#include <sys/stat.h>
#include <Windows.h> // This reduces portability to windows and needs a more general fix using either boost or c++ 17

std::string TSP_Solver::export_to_file(std::vector<std::pair<int, std::vector<int>>> routes, std::string prefix) {
	std::string DIR = "Results";
	std::string CREATE_DIR_COMMAND = "mkdir " + DIR;
	std::string SUFFIX = "_graph.txt";
	std::string filename = DIR + "\\" + prefix + SUFFIX;
	struct stat buf;
	int file_count = 1;

	// Running make directory command and supressing command line output
	system((CREATE_DIR_COMMAND + " >nul 2>nul").c_str()); 

	while (stat(filename.c_str(), &buf) != -1) {
		filename = DIR + "\\" + prefix + "_" + std::to_string(file_count) + SUFFIX;
		++file_count;
	}
	
	std::ofstream file(filename);
	for (auto route : routes) {
		file << "DISTANCE: " + std::to_string(route.first) << std::endl;
		for (int city : route.second) {
			file << std::to_string(city) << std::endl;
		}
		file << "END" << std::endl;
	}
	return filename;
}

void TSP_Solver::test_euclidean_dist() {
	std::string DATA_URL = "http://elib.zib.de/pub/mp-testdata/tsp/tsplib/tsp/att48.tsp";
	std::string OPTIMAL_TOUR = "http://elib.zib.de/pub/mp-testdata/tsp/tsplib/tsp/att48.opt.tour";

	Data_Collection data_manipulator = Data_Collection(&DATA_URL, &OPTIMAL_TOUR);
	Route optimal_route = Route(data_manipulator.optimal_route, &data_manipulator);

	std::cout << optimal_route.distance << std::endl;
}

// Carries out the two-opt algorithm upon the provided route given the indexes for the boundaries of the algorithm
std::vector<int> TSP_Solver::two_opt(const std::vector<int>* route, int city_index_1, int city_index_2) {
	std::vector<int> new_route;

	for (int i = 0; i < city_index_1; ++i) {
		new_route.push_back((*route)[i]);
	}

	for (int i = city_index_2; i >= city_index_1; --i) {
		new_route.push_back((*route)[i]);
	}

	for (unsigned int i = city_index_2 + 1; i < route->size(); ++i) {
		new_route.push_back((*route)[i]);
	}

	return new_route;
}

/* Generates a vector containing a random permutation of digits up to the provided solution size.
The vector contains no duplicate digits*/
std::vector<int> TSP_Solver::generate_rand_solution(int solution_size) {
	std::random_device rd;
	std::mt19937 eng(rd());
	std::vector<int> solution;

	for (int i = 1; i < solution_size + 1; ++i) {
		solution.push_back(i);
	}

	std::shuffle(solution.begin(), solution.end(), eng);
	return solution;
}
