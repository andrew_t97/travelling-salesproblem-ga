#pragma once
#include <vector>
#include <string>


class Data_Collection {
private:
	std::vector<std::pair<int, int>> coord_data;
	static size_t writeFunction(void* ptr, size_t size, size_t nmemb, std::string* data);
	std::string get_data(const std::string* url);
	bool is_digits_or_spaces(const char* buffer);
	std::vector<std::pair<int, int>> get_coordinates(const std::string* data);
	std::vector<int> get_optimal_route(const std::string* data);
	int calc_euclidean_distance(const std::pair<int, int>* a, std::pair<int, int>* b);

public:
	std::vector<int> optimal_route;
	int calc_route_distance(const std::vector<int>* tour_ptr);
	Data_Collection(const std::string* coordinate_data_url, const std::string* optimal_route_url);
};